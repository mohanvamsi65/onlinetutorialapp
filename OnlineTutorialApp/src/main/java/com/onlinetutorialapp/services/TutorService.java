
package com.onlinetutorialapp.services;

import java.util.List;

import com.onlinetutorialapp.entities.Tutor;

public interface TutorService {

	public boolean createTutor(int user_id, Tutor tutor);

	public List<Tutor> findByCategory(String category);

	/**
	 * this method is used to get the tutor object by tutor id
	 * 
	 * @param id
	 * @return
	 */
	public Tutor filterById(int tutor_id);
}
