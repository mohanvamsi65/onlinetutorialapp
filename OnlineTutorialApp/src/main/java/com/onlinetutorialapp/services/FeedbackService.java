package com.onlinetutorialapp.services;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.onlinetutorialapp.dao.FeedbackDAO;
import com.onlinetutorialapp.entities.Feedback;

@Service
@Transactional
public class FeedbackService {
	@Autowired
	private FeedbackDAO feedbackDao;

	public int save(Feedback feedback) {

		Feedback feedbacks = feedbackDao.save(feedback);
		if (feedbacks != null) {

			System.out.println(feedbacks);

			return 1;

		}
		return -1;

	}

	public List<Feedback> findAll() {

		List<Feedback> feedbackList = new ArrayList<Feedback>();

		Iterable<Feedback> feedback = feedbackDao.findAll();
		Iterator<Feedback> iterator = feedback.iterator();

		while (iterator.hasNext()) {
			feedbackList.add(iterator.next());
		}
		return feedbackList;
	}

}