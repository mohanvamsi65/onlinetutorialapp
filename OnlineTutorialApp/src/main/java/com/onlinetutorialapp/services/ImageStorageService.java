package com.onlinetutorialapp.services;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

@Service
public class ImageStorageService {

	private final Path rootLocation = Paths.get("./images");

	@SuppressWarnings("resource")
	
	public String store(MultipartFile file) throws Exception {
		
		final String imagePath = "images/"; // path
		String[] imageNames = (file.getOriginalFilename().split("\\\\"));
		String imageName = imagePath + imageNames[imageNames.length - 1];
	
		FileOutputStream output;
		output = new FileOutputStream(imageName);
		output.write(file.getBytes());

		return imageName;   

//		
//		try {
//			long insertedlength = Files.copy(file.getInputStream(), this.rootLocation.resolve(file.getOriginalFilename()));
//		    if(insertedlength > 0) {
//		    	return "images/"+file.getOriginalFilename();
//		    }
//		    else {
//		    	return "Not inserted";
//		    }
//		} catch (Exception e) {
//			throw new RuntimeException("FAIL!");
//		}
//		
	}

	public byte[] loadFile(String filename) throws Exception{
		
		try {
			Path file = rootLocation.resolve(filename);
			
//			  byte[] bytes = FileUtils.copyToByteArray(imgFile.getInputStream());
//			  File file1 = ResourceUtils.getFile(filename);
			byte[] bytes = Files.readAllBytes(file);
			return bytes;
		} catch (IOException e) {
			throw new RuntimeException("FAIL!");
		}
		

//
//			if (resource.exists() || resource.isReadable()) {
//				return resource;
//			} else {
//				throw new RuntimeException("FAIL!");
//			}
//		} catch (MalformedURLException e) {
//			throw new RuntimeException("FAIL!");
//		}
	}

	public void deleteAll() {
		
		FileSystemUtils.deleteRecursively(rootLocation.toFile());
	}

	public void init() {
		
		try {
			Files.createDirectory(rootLocation);
		} catch (java.io.IOException e) {
			throw new RuntimeException("Could not initialize storage!");
		}
	}
}