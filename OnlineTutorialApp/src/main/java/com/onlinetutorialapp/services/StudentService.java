package com.onlinetutorialapp.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.onlinetutorialapp.dao.StudentDAO;
import com.onlinetutorialapp.dao.UserDAO;
import com.onlinetutorialapp.entities.Student;

@Service
@Transactional
public class StudentService {

	@Autowired
	private StudentDAO studentDAO;
	public Object responseType;
	@Autowired
	private UserDAO userDAO;

	public List<Student> getAllStudents() {

		return (List<Student>) studentDAO.findAll();
	}
	
	
}
