
package com.onlinetutorialapp.services;

import java.util.ArrayList;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.onlinetutorialapp.dao.SubjectDAO;
import com.onlinetutorialapp.dao.SubjectVideoDAO;
import com.onlinetutorialapp.entities.Sub_Videos;
import com.onlinetutorialapp.entities.Subject;

@Service
@Transactional
public class SubjectService {

	@Autowired
	private SubjectDAO subjectDAO;

	@Autowired
	private SubjectVideoDAO subjectVideoDao;

	public List<Subject> findBySubjectName(String subject_name) throws Exception {
		List<Subject> subjects = new ArrayList<Subject>();
		List<Subject> SubjectList = subjectDAO.findBySubjectName(subject_name);
		System.out.println("SubjectList " + SubjectList);

		return SubjectList;

	}

	public int addSubjectInfo(Subject subject) {

		Subject sub = subjectDAO.save(subject);
		if (sub != null) {
			return 1;
		} else {
			return -1;
		}

	}

	public int addSubjectVideos(Sub_Videos sub_videos) {

		Sub_Videos subvideo = subjectVideoDao.save(sub_videos);
		if (subvideo != null) {
			return 1;
		} else {
			return -1;
		}

	}
}
