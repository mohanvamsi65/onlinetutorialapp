
package com.onlinetutorialapp.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.onlinetutorialapp.dao.TutorDAO;
import com.onlinetutorialapp.dao.UserDAO;
import com.onlinetutorialapp.entities.Tutor;
import com.onlinetutorialapp.entities.User;

@Service
@Transactional
public class TutorServiceImpl implements TutorService {
	@Autowired
	private UserDAO userDao;
	@Autowired
	private TutorDAO tutorDao;

	public boolean createTutor(int user_id, Tutor tutor) {

		try {
			User user_tutor = (User) userDao.findById(user_id).get();
			if (user_tutor.getRole() == 2) {

				tutor.setTutor(user_tutor);
				tutorDao.save(tutor);
				return true;
			}
		} catch (Exception e) {
			return false;
		}
		return false;
	}

	public List<Tutor> findByCategory(String category) {
		List<Tutor> com = new ArrayList<Tutor>();
		List<Tutor> tutorList = tutorDao.findByCategory(category);
		for (Tutor tutor : tutorList) {
			System.out.println(tutor);
			if (tutor.getCategory().startsWith(category)) {
				com.add(tutor);
				System.out.println(tutor.getCategory());
			} else
				System.out.println(tutor.getCategory());
		}

		return com;

	}

	@Override
	public Tutor filterById(int tutor_id) {
		Optional<Tutor> tutor = tutorDao.findById(tutor_id);
		return tutor.get();
	}

}
