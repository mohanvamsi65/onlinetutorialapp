package com.onlinetutorialapp.services;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import com.onlinetutorialapp.dao.UserDAO;
import com.onlinetutorialapp.dto.SecurityUserDetails;
import com.onlinetutorialapp.entities.User;


@Service
@Transactional
public class UserService implements UserDetailsService{
  
	@Autowired
	private UserDAO userDAO;
	
	public int userRegistration(User user) {
		User userDb = userDAO.findByEmail(user.getEmail());
		if (userDb == null) {
			userDAO.save(user);
			return 1;
		}
		return -1;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userDAO.findByEmail(username);
		if (user == null) {
			throw new UsernameNotFoundException("User not registered");
		}

		return new SecurityUserDetails(user);

	}
	public User editUserInfo(User user) throws Exception { 
		return userDAO.save(user);
	
	}
	
}
