package com.onlinetutorialapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.onlinetutorialapp.entities.Feedback;
import com.onlinetutorialapp.services.FeedbackService;
import com.onlinetutorialapp.util.ResponseType;
import com.onlinetutorialapp.util.StatusCodes;

@RestController
public class FeedbackController {
	@Autowired
	private FeedbackService feedbackService;
	@Autowired
	private ResponseType responseType;

	@PostMapping("/postFeedback")
	public ResponseEntity<ResponseType> postFeedback(@RequestBody Feedback feedback) {

		int res = feedbackService.save(feedback);
		String status = "";

		if (res == 1) {
			//status = feedback.getFeedback_id();
			status = "success";
			responseType.setResponse(StatusCodes.OK.getValue());
		} else {
			status = "not saved";
			responseType.setResponse(StatusCodes.ERROR.getValue());
		}
		responseType.setResponse(status);

		return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);

	}

	@GetMapping("/getAllFeedbacks")
	public ResponseEntity<List<Feedback>> list() {
		List<Feedback> accounts = feedbackService.findAll();
		if (accounts.isEmpty()) {
			return new ResponseEntity<List<Feedback>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Feedback>>(accounts, HttpStatus.OK);
	}

}