package com.onlinetutorialapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.onlinetutorialapp.entities.Student;
import com.onlinetutorialapp.services.StudentService;
import com.onlinetutorialapp.util.ResponseType;

@CrossOrigin(origins ="*")
@RestController
public class StudentController {
	
	@Autowired
	private StudentService studentService;
	@Autowired
	private ResponseType responseType;
	
	/**
	 * This method is to display all students.
	 * @return
	 */
	@GetMapping("/displayAllStudents")
	public ResponseEntity<ResponseType> displayAllStudents() {
		List<Student> allStudents = studentService.getAllStudents();
		responseType.setResponse(allStudents);
		return new ResponseEntity<ResponseType>(responseType,HttpStatus.OK);
}
	
	
}
