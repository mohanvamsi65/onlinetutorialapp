
package com.onlinetutorialapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.onlinetutorialapp.entities.User;
import com.onlinetutorialapp.services.UserService;
import com.onlinetutorialapp.util.ResponseType;
import com.onlinetutorialapp.util.StatusCodes;

@RestController
@CrossOrigin
public class UserController {

	@Autowired
	private UserService userService;

	@Autowired
	ResponseType responseType;
    /**
     * This method is for UserRegistration with his details.
     * @param user
     * @return
     * @throws Exception
     */
	@PostMapping("/userRegistration")
	public ResponseEntity<ResponseType> userRegistration(@RequestBody User user) throws Exception {

		int result = userService.userRegistration(user);
		String status = "";
		if (result == 1) {
			status = "success";
			responseType.setStatusCode(StatusCodes.OK.getValue());
		} else {
			status = "Email already Exits";
			responseType.setStatusCode(StatusCodes.ERROR.getValue());
		}
		responseType.setResponse(status);

		return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
	}
	
	
    /**
     * This method is to update the UserDetails.
     * @param user
     * @return
     * @throws Exception
     */
	@PutMapping("api/editUserInfo")
	public ResponseEntity<ResponseType> editUserInfo(@RequestBody User user) throws Exception{

		User userStatus = null;
		try {

			userStatus = userService.editUserInfo(user);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (userStatus != null) {
			responseType.setResponse(user);
			responseType.setStatusCode(StatusCodes.OK.getValue());
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		} else {
			responseType.setResponse(StatusCodes.NOT_FOUND);
			responseType.setStatusCode(StatusCodes.NOT_FOUND.getValue());
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		}

	}
}
