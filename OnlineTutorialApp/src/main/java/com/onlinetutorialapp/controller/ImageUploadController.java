package com.onlinetutorialapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.onlinetutorialapp.services.ImageStorageService;
import com.onlinetutorialapp.util.ResponseType;
import com.onlinetutorialapp.util.StatusCodes;


@CrossOrigin
@RestController
public class ImageUploadController {
	@Autowired
	ResponseType responseType;

	@Autowired
	ImageStorageService imageStorageService;

	/**
	 * this method uploads the image and returns the image path as a url
	 * 
	 * @param images
	 * @return
	 */
	@CrossOrigin
	@PostMapping("/api/uploadImage")
	public ResponseEntity<ResponseType> uploadImage(@RequestParam(name = "fileName") MultipartFile image) {
		try {

			// URL url = service.uploadImage(image);

			String location = imageStorageService.store(image);
			responseType.setResponse(location);
			responseType.setStatusCode(StatusCodes.OK.getValue());

		} catch (Exception e) {
			responseType.setResponse("upload  Failed");
			responseType.setStatusCode(StatusCodes.ERROR.getValue());
		}

		return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);

	}

	@CrossOrigin
	@GetMapping(value = "/images/{imageId}", produces = MediaType.IMAGE_JPEG_VALUE)
	public ResponseEntity<byte[]> downloadImage(@PathVariable("imageId") String imageId) {
		byte[] file = null;
		try {
			file = imageStorageService.loadFile(imageId);
		} catch (Exception e) {
			
		}
		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\""  + "\"")
				.body(file);
	}
}
