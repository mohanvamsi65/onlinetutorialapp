
package com.onlinetutorialapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.onlinetutorialapp.entities.Tutor;
import com.onlinetutorialapp.services.TutorService;
import com.onlinetutorialapp.util.ResponseType;
import com.onlinetutorialapp.util.StatusCodes;


@RestController
@CrossOrigin
public class TutorController {
   
	@Autowired
	private TutorService tutorService;
	
	@Autowired
	ResponseType  responseType;
	
	/**
	 * This method is to create tutor with his details.
	 * @param userid
	 * @param tutor
	 * @return
	 */
	@PostMapping("/create/tutors/{user_id}")
	public ResponseEntity<ResponseType> createTutor(@PathVariable("user_id") int userid, @RequestBody Tutor tutor) {

		boolean status = false;
	
		try {
			status = tutorService.createTutor(userid, tutor);
		} catch (Exception e) {
			e.printStackTrace();
			status = false;
		}

		if (status) {
			responseType.setResponse("tutor created Successfully");
			responseType.setStatusCode(StatusCodes.OK.getValue());
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		} else {
			responseType.setResponse("Some thing wrong");
			responseType.setStatusCode(StatusCodes.ERROR.getValue());
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		}

	}
	
	/**
	 * This method is to get the tutors based on the category(Online/Offline)
	 * @param category
	 * @return
	 */
	@GetMapping("/getbycategory/{category}")
	public ResponseEntity<ResponseType> getByCategory(@PathVariable("category") String category) {

		java.util.List<Tutor> list = tutorService.findByCategory(category);

		responseType.setResponse(list);

		return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
	}
	
   
	/**
	 * this methods calls the service package for displaying the tutor based on
	 * tutor_id
	 * @param id
	 * @return
	 */
	@GetMapping("/searchTutorById/{tutor_id}")
	public ResponseEntity<Tutor> getByItemId(@PathVariable("tutor_id") int tutor_id) {
		Tutor tutor = tutorService.filterById(tutor_id);
		if (tutor == null) {

			return new ResponseEntity<Tutor>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Tutor>(tutor, HttpStatus.OK);
	}

	
}

