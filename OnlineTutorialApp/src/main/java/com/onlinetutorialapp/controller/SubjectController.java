package com.onlinetutorialapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.onlinetutorialapp.entities.Sub_Videos;
import com.onlinetutorialapp.entities.Subject;
import com.onlinetutorialapp.services.SubjectService;
import com.onlinetutorialapp.util.ResponseType;
import com.onlinetutorialapp.util.StatusCodes;

@RestController
@CrossOrigin
public class SubjectController {

	@Autowired
	ResponseType responseType;
	@Autowired
	private SubjectService subjectService;
    
	/**
	 * This method is to get the sub_videos,sub_materials with the sub_name.
	 * 
	 * @param subject_name
	 * @return
	 * @throws Exception
	 */
	@GetMapping("/getbysubjectname/{subject_name}")
	public ResponseEntity<ResponseType> getBySubjectName(@PathVariable("subject_name") String subject_name)
			throws Exception {

		java.util.List<Subject> list = subjectService.findBySubjectName(subject_name);
		if (list.size() > 0) {
			System.out.println("SubjectList "+ list);
			responseType.setResponse(list);
			responseType.setStatusCode(StatusCodes.OK.getValue());
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		} else {
			responseType.setResponse(StatusCodes.NOT_FOUND);
			responseType.setStatusCode(StatusCodes.NOT_FOUND.getValue());
			return new ResponseEntity<ResponseType>(responseType, HttpStatus.OK);
		}
	}
	
//	@PostMapping("/addsubject")
//	public ResponseEntity<ResponseType> addSubjectInfo(@RequestBody Subject subject) {
//		String status = "";
//		int result=0;
//		try {
//		result = subjectService.addSubjectInfo(subject);
//		} catch(Exception e){
//			status = "Subject Already Exits";
//		    responseType.setStatusCode(StatusCodes.ERROR.getValue());
//		}
//			
//	if(result == 1) {
//		status = "success";
//		responseType.setStatusCode(StatusCodes.OK.getValue());
//			} else {
//			    status = "subject Already Exits";
//			    responseType.setStatusCode(StatusCodes.ERROR.getValue());
//			
//		}
//		responseType.setResponse(status);
//		
//		return new ResponseEntity<ResponseType>(responseType,HttpStatus.OK);
//	}

	 /**
	  * This method is used to add subjects Information.
	  * @param subject
	  * @return
	  */
	 @PostMapping("/addSubject")
	 public ResponseEntity<ResponseType> addSubjectInfo(@RequestBody Subject subject) {
		int result =  subjectService.addSubjectInfo(subject);
		
		
		String status = "";
			if(result == 1) {
				status = "success";
				responseType.setStatusCode(StatusCodes.OK.getValue());
			} else {
			    status = "already Exits";
			    responseType.setStatusCode(StatusCodes.ERROR.getValue());
			}
			responseType.setResponse(status);
			return new ResponseEntity<ResponseType>(responseType,HttpStatus.OK);
	  }
	 
	 
	 /**
	  * This method is used to add subjects Information.
	  * @param subject
	  * @return
	  */
	 @PostMapping("/addSubjectVideos")
	 public ResponseEntity<ResponseType> addSubjectInfo(@RequestBody Sub_Videos sub_videos) {
		 
		int result =  subjectService.addSubjectVideos(sub_videos);
		
		
		String status = "";
			if(result == 1) {
				status = "success";
				responseType.setStatusCode(StatusCodes.OK.getValue());
			} else {
			    status = "already Exits";
			    responseType.setStatusCode(StatusCodes.ERROR.getValue());
			}
			responseType.setResponse(status);
			return new ResponseEntity<ResponseType>(responseType,HttpStatus.OK);
	  }
}

