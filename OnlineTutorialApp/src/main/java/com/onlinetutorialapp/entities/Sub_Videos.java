
package com.onlinetutorialapp.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;



@Entity
@Table(name = "sub_videos_tbl")
public class Sub_Videos {
	@Id
	@GenericGenerator(name = "IdGen", strategy = "com.onlinetutorialapp.util.IdGenerator")
	@GeneratedValue(generator = "IdGen")
	private int sub_videos_id;
	private String sub_video_name;

	@ManyToOne
	@JoinColumn(name = "subject_id")
	private Subject subjects;

	@ManyToOne
	@JoinColumn(name = "tutor_id")
	private Tutor tutors;

	private String subject_videos;

	public int getSub_videos_id() {
		return sub_videos_id;
	}

	public void setSub_videos_id(int sub_videos_id) {
		this.sub_videos_id = sub_videos_id;
	}

	public String getSub_video_name() {
		return sub_video_name;
	}

	public void setSub_video_name(String sub_video_name) {
		this.sub_video_name = sub_video_name;
	}

	public Subject getSubjects() {
		return subjects;
	}

	public void setSubjects(Subject subjects) {
		this.subjects = subjects;
	}

	public Tutor getTutors() {
		return tutors;
	}

	public void setTutors(Tutor tutors) {
		this.tutors = tutors;
	}

	public String getSubject_videos() {
		return subject_videos;
	}

	public void setSubject_videos(String subject_videos) {
		this.subject_videos = subject_videos;
	}

	
}
