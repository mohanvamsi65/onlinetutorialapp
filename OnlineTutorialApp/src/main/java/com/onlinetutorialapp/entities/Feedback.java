package com.onlinetutorialapp.entities;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name = "feedback_tbl")
public class Feedback {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int feedback_id;
	private String skill;
	private int skill_category;
	private int max_rating;
	private int cutoff_rating;
	private int rating;
	private String comment;
	
	
	@OneToOne
	@JoinColumn(name = "student_id")
	private Student student;
	
	@ManyToOne(cascade = {CascadeType.MERGE},fetch= FetchType.EAGER)
	@JoinColumn(name = "tutor_id")
	private Tutor tutors ;
	
	public int getFeedback_id() {
		return feedback_id;
	}
	public void setFeedback_id(int feedback_id) {
		this.feedback_id = feedback_id;
	}
	
	public Tutor getTutors() {
		return tutors;
	}
	public void setTutors(Tutor tutors) {
		this.tutors = tutors;
	}
	public Student getStudent() {
		return student;
	}
	public void setStudent(Student student) {
		this.student = student;
	}
	public String getSkill() {
		return skill;
	}
	public void setSkill(String skill) {
		this.skill = skill;
	}
	public int getSkill_category() {
		return skill_category;
	}
	public void setSkill_category(int skill_category) {
		this.skill_category = skill_category;
	}
	public int getMax_rating() {
		return max_rating;
	}
	public void setMax_rating(int max_rating) {
		this.max_rating = max_rating;
	}
	public int getCutoff_rating() {
		return cutoff_rating;
	}
	public void setCutoff_rating(int cutoff_rating) {
		this.cutoff_rating = cutoff_rating;
	}
	public int getRating() {
		return rating;
	}
	public void setRating(int rating) {
		this.rating = rating;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	

}
