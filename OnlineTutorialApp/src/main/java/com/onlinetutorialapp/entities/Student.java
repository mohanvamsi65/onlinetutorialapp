package com.onlinetutorialapp.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "student_tbl")
public class Student {

	@Id
	@GenericGenerator(name = "IdGen", strategy = "com.onlinetutorialapp.util.IdGenerator")
	@GeneratedValue(generator = "IdGen")
	private int student_id;
	@OneToOne
	private Feedback feedBack;

	@ManyToMany(mappedBy = "students", cascade = { CascadeType.ALL })
	private List<Tutor> tutor;

	@OneToOne
	private User user;

	public int getStudent_id() {
		return student_id;
	}

	public void setStudent_id(int student_id) {
		this.student_id = student_id;
	}

	public Feedback getFeedBack() {
		return feedBack;
	}

	public void setFeedBack(Feedback feedBack) {
		this.feedBack = feedBack;
	}

	public List<Tutor> getTutor() {
		return tutor;
	}

	public void setTutor(List<Tutor> tutor) {
		this.tutor = tutor;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}


}
