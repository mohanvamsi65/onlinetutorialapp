
package com.onlinetutorialapp.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "tutor_tbl")
public class Tutor {
	@Id
	@GenericGenerator(name = "IdGen", strategy = "com.onlinetutorialapp.util.IdGenerator")
	@GeneratedValue(generator = "IdGen")
	private int tutor_id;
	private String heading;
	private String description;
	private String availability_time;
	private String tutorDemo_video;
	private String category;

    @OneToOne
    private User user;

    @ManyToMany(targetEntity=Student.class, cascade = {CascadeType.ALL})
    @JoinTable(name = "tutor_student_tbl", joinColumns = { @JoinColumn(name = "tutor_id") }, inverseJoinColumns = {
			@JoinColumn(name = "student_id") })
	private List<Student> students;

	@OneToMany(mappedBy = "tutors", cascade = {CascadeType.MERGE},fetch= FetchType.EAGER)
	private List<Feedback> feeback;
    
	@ManyToOne
	@JoinColumn(name = "subject_id")
	@JsonIgnore
	private Subject subject;
	
	@OneToMany(mappedBy = "tutors", cascade = CascadeType.ALL)
	private List<Sub_Videos> sub_videos;
	
	public int getTutor_id() {
		return tutor_id;
	}

	public void setTutor_id(int tutor_id) {
		this.tutor_id = tutor_id;
	}

	public String getHeading() {
		return heading;
	}

	public void setHeading(String heading) {
		this.heading = heading;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAvailability_time() {
		return availability_time;
	}

	public void setAvailability_time(String availability_time) {
		this.availability_time = availability_time;
	}

	public String getTutorDemo_video() {
		return tutorDemo_video;
	}

	public void setTutorDemo_video(String tutorDemo_video) {
		this.tutorDemo_video = tutorDemo_video;
	}
   
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
	
	public User getTutor() {
		return user;
	}

	public void setTutor(User user) {
		this.user = user;
	}

	public List<Student> getStudents() {
		return students;
	}

	public void setStudents(List<Student> students) {
		this.students = students;
	}

}
