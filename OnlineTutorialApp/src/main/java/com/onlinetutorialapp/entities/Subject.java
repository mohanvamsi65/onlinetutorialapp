
package com.onlinetutorialapp.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "subject_tbl")
public class Subject {
	@Id
	@GenericGenerator(name = "IdGen",strategy = "com.onlinetutorialapp.util.IdGenerator")
	@GeneratedValue(generator = "IdGen")
	private int subject_id;
	
	private String Subject_name;
	
	private String sub_material;
	

	@OneToMany(mappedBy = "subjects",cascade = { CascadeType.ALL })
	private List<Sub_Videos> videos;


	public int getSubject_id() {
		return subject_id;
	}


	public void setSubject_id(int subject_id) {
		this.subject_id = subject_id;
	}


	public String getSubject_name() {
		return Subject_name;
	}


	public void setSubject_name(String subject_name) {
		Subject_name = subject_name;
	}


	public String getSub_material() {
		return sub_material;
	}


	public void setSub_material(String sub_material) {
		this.sub_material = sub_material;
	}


	public List<Sub_Videos> getVideos() {
		return videos;
	}


	public void setVideos(List<Sub_Videos> videos) {
		this.videos = videos;
	}

}

