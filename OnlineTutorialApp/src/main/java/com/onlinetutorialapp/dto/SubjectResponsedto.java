package com.onlinetutorialapp.dto;

public class SubjectResponsedto {
	
    private int subject_id;
	private String Subject_name;
	private String sub_material;
	private String sub_video_name;
    private String sub_videos;
	
	public String getSub_videos() {
		return sub_videos;
	}

	public void setSub_videos(String sub_videos) {
		this.sub_videos = sub_videos;
	}

	public int getSubject_id() {
		return subject_id;
	}

	public void setSubject_id(int subject_id) {
		this.subject_id = subject_id;
	}

	public String getSubject_name() {
		return Subject_name;
	}

	public void setSubject_name(String subject_name) {
		Subject_name = subject_name;
	}

	public String getSub_material() {
		return sub_material;
	}

	public void setSub_material(String sub_material) {
		this.sub_material = sub_material;
	}

	public String getSub_video_name() {
		return sub_video_name;
	}

	public void setSub_video_name(String sub_video_name) {
		this.sub_video_name = sub_video_name;
	}
	

}
