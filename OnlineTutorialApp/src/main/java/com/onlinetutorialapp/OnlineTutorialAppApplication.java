package com.onlinetutorialapp;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.web.firewall.DefaultHttpFirewall;
import org.springframework.security.web.firewall.HttpFirewall;

import com.onlinetutorialapp.services.ImageStorageService;

@SpringBootApplication
public class OnlineTutorialAppApplication {
     
	
	@Autowired
	ImageStorageService imageStorageService;
	
	public static void main(String[] args) {
		SpringApplication.run(OnlineTutorialAppApplication.class, args);
	}
  
	@Bean
	public ModelMapper getModelMapper() {
		return new ModelMapper();
	}
	
	@Bean
	public HttpFirewall defaultHttpFirewall() {
	    return new DefaultHttpFirewall();
	}
}
