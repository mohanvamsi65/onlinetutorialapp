package com.onlinetutorialapp.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.onlinetutorialapp.entities.Sub_Videos;

public interface SubjectVideoDAO extends JpaRepository<Sub_Videos, Integer>{

}
