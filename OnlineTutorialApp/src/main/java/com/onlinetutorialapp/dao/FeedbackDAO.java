package com.onlinetutorialapp.dao;

import org.springframework.data.repository.CrudRepository;

import com.onlinetutorialapp.entities.Feedback;

public interface FeedbackDAO  extends CrudRepository<Feedback, Integer>{


}
