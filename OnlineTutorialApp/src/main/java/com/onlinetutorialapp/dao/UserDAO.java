package com.onlinetutorialapp.dao;


import java.util.List;
import org.springframework.data.repository.CrudRepository;

import com.onlinetutorialapp.entities.Tutor;
import com.onlinetutorialapp.entities.User;

public interface UserDAO extends CrudRepository<User, Integer> {
	
	public User findByEmail(String email);

	public List<User> findByRole(int role);
 
	//public User save(User user);

	public void save(Tutor tutor);
	
}
