
package com.onlinetutorialapp.dao;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.onlinetutorialapp.entities.Subject;
import com.onlinetutorialapp.entities.Tutor;
import com.onlinetutorialapp.entities.User;

public interface TutorDAO extends CrudRepository<Tutor, Integer> {

	public Tutor save(User tutor);

	public List<Tutor> findByCategory(String category);

	@Query(value = "select * from tutor_tbl where heading=?", nativeQuery = true)
	public List<Tutor> findByHeading(String heading);

	public void save(Subject subject);

}
