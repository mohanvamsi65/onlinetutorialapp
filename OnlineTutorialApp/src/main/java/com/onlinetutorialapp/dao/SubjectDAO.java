
package com.onlinetutorialapp.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.onlinetutorialapp.entities.Sub_Videos;
import com.onlinetutorialapp.entities.Subject;

public interface SubjectDAO extends CrudRepository<Subject, Integer>{
    
	@Query("from Subject where subject_name like concat(subject_name,'%')")
	public List<Subject> findBySubjectName(String subject_name);

	public Sub_Videos save(Sub_Videos sub_videos);

}


