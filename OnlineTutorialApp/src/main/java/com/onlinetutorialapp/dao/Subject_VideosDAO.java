package com.onlinetutorialapp.dao;

import org.springframework.data.repository.CrudRepository;

import com.onlinetutorialapp.entities.Sub_Videos;


public interface Subject_VideosDAO extends CrudRepository<Subject_VideosDAO, Integer> {

	Sub_Videos save(Sub_Videos sub_videos);

}
