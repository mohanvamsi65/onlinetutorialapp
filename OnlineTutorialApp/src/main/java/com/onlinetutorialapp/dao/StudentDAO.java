package com.onlinetutorialapp.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.onlinetutorialapp.entities.Student;


public interface StudentDAO extends CrudRepository<Student, Integer> { 
	
	@Query("from Student where student_id =:student_id")
	public List<Student> findStudentDetails(int student_id);

}
 