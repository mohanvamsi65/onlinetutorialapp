package com.onlinetutorialapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.onlinetutorialapp.entities.User;

public interface UserRepository extends JpaRepository<User,Integer>{
	
	

}
