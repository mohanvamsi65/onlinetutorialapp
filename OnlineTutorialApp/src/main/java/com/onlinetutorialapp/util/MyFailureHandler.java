package com.onlinetutorialapp.util;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import com.fasterxml.jackson.databind.ObjectMapper;

public class MyFailureHandler implements AuthenticationFailureHandler{
    
	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException exception) throws IOException, ServletException {
		ResponseType responseType = new ResponseType();
		responseType.setResponse(exception.getMessage());
		responseType.setStatusCode(StatusCodes.NOT_AUTH.getValue());
		response.getWriter().print(new ObjectMapper().writeValueAsString(responseType));
	}

}
