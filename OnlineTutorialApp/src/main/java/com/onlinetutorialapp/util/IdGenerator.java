
package com.onlinetutorialapp.util;

import java.io.Serializable;

import java.sql.Connection;
import java.sql.ResultSet;

import java.sql.Statement;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;

/**
 * this class is used to create a auto generated values for the all auto
 * increment variables
 * 
 * @author IMVIZAG
 *
 */

public class IdGenerator implements IdentifierGenerator {

	@Override
	public Serializable generate(SharedSessionContractImplementor session, Object object) throws HibernateException {

		Connection con = session.connection();
		Integer newId = 0;
		ResultSet rs = null;
		Statement st = null;
		int prefix = 0;
		try {
			st = con.createStatement();
			switch (object.getClass().getName()) {
			case "com.onlinetutorialapp.entities.User":
				prefix = 1001;
				rs = st.executeQuery("select max(user_id) as max from user_tbl");
				break;

			case "com.onlinetutorialapp.entities.Tutor":
				prefix = 2001;
				rs = st.executeQuery("select max(tutor_id) as max from tutor_tbl");
				break;

			case "com.onlinetutorialapp.entities.Student":
				prefix = 3001;
				rs = st.executeQuery("select max(student_id) as max from student_tbl");
				break;

			case "com.onlinetutorialapp.entities.Feedback":
				prefix = 4001;
				rs = st.executeQuery("select max(feedback_id) as max from feedback_tbl");
				break;

			case "com.onlinetutorialapp.entities.Subject":
				prefix = 5001;
				rs = st.executeQuery("select max(subject_id) as max from subject_tbl");
				break;

			case "com.onlinetutorialapp.entities.Sub_Videos":
				prefix = 6001;
				rs = st.executeQuery("select max(sub_videos_id) as max from sub_videos_tbl");

				break;

			case "com.onlinetutorialapp.entities.Admin":
				prefix = 7001;
				rs = st.executeQuery("select max(admin_id) as max from admin_tbl");

				break;
			default:
				break;
			}

			if (rs.next()) {
				newId = rs.getInt("max") == 0 ? rs.getInt("max") + prefix : rs.getInt("max") + 1;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return newId;
	}

}
