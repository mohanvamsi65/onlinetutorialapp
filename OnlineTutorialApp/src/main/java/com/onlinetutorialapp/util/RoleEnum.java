package com.onlinetutorialapp.util;


public enum RoleEnum {
	
	
	Admin(1), Tutor(2), Student(3);
	
	int value;
	
    private RoleEnum(int value) {
		this.value = value;
	}
	
	public int getValue(int value) {
		return value;
	}
	
	
}
