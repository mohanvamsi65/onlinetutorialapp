package com.onlinetutorialapp.util;

/**
 * this class is used to set the status codes
 * @author IMVIZAG
 *
 */
public enum StatusCodes {
      OK(1),
      ERROR(2),
      NOT_FOUND(3),
      ALREADY_EXIST(4),
      NOT_AUTH(5),
      NOT_REGISTER(6);
	  
	private int value;
	 
	private StatusCodes(final int value) {
		this.value = value;
	}
	
	public int getValue() {
		return value;
	}

  
}
