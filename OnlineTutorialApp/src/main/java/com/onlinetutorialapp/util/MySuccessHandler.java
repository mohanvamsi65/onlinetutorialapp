package com.onlinetutorialapp.util;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.onlinetutorialapp.dto.SecurityUserDetails;
import com.onlinetutorialapp.entities.User;
import com.onlinetutorialapp.services.UserService;


@Component
public class MySuccessHandler implements AuthenticationSuccessHandler {
    
//	@Autowired
//	UserService userService;
	
    UserService userService = new UserService();
    
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {
		ResponseType responseType = new ResponseType();
		System.out.println(request.getParameter("username"));
		  
		SecurityUserDetails userDetails = (SecurityUserDetails) authentication.getPrincipal();
		User user = userDetails.getUser();
		responseType.setResponse(user);
		responseType.setStatusCode(StatusCodes.OK.getValue());
		response.getWriter().print(new ObjectMapper().writeValueAsString(responseType));
		
	}

}
